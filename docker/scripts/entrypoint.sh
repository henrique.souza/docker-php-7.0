#!/bin/bash

APP_PATH=/var/www/html
VENDOR_DIR=$APP_PATH/vendor
VENDOR_DIR_EXISTS=false
if [ -d "$VENDOR_DIR" ]; then
    VENDOR_DIR_EXISTS=true
fi
if [ "$VENDOR_DIR_EXISTS" = "false" ]; then

  echo "Iniciando build..."
  sleep 2
  cd $APP_PATH
  composer install
fi

cd $APP_PATH
echo "** Iniciando nginx/php-fpm..."
service php7.0-fpm start
service php7.0-fpm stop
exec /usr/bin/supervisord -n -c /etc/supervisord.conf

echo "** Build concluído"
