FROM henriqueflaviodev/oracle-client-11c:1.0

#Supervisor
RUN apt update && \
    apt install -y supervisor
ADD docker/conf/supervisord.conf /etc/supervisord.conf
RUN mkdir -p /var/log/supervisor

#Nginx
RUN apt update && \
    apt install -y nginx

RUN systemctl enable nginx.service

RUN useradd -s /bin/false nginx

ADD docker/conf/nginx.conf /etc/nginx/nginx.conf

ADD docker/conf/nginx-supervisord.conf /tmp/supervisord.conf
RUN echo "\n" >> /etc/supervisord.conf && \
    cat /tmp/supervisord.conf >> /etc/supervisord.conf && \
    rm -f /tmp/supervisord.conf

EXPOSE 443 80

ENV APP_PATH=/var/www/html
WORKDIR $APP_PATH

#Packages
RUN apt update && \
    apt upgrade -y && \
    apt install -y \
    ca-certificates \
    lsb-release \
    apt-transport-https \
    zip \
    unzip

#PHP
RUN apt update && \
    apt upgrade -y ca-certificates

RUN echo "deb https://packages.sury.org/php/ `lsb_release -cs` main"  \
    | tee /etc/apt/sources.list.d/sury-php.list

RUN curl -fsSL https://packages.sury.org/php/apt.gpg | apt-key add -

ENV PHP_VERSION=7.0
RUN apt update && \
    apt install -y \
        php$PHP_VERSION-fpm \
        php$PHP_VERSION-common \
        php$PHP_VERSION-gmp \
        php$PHP_VERSION-curl \
        php$PHP_VERSION-intl \
        php$PHP_VERSION-mbstring \
        php$PHP_VERSION-xmlrpc \
        php$PHP_VERSION-gd \
        php$PHP_VERSION-xml \
        php$PHP_VERSION-cli \
        php$PHP_VERSION-zip \
        php$PHP_VERSION-soap \
        php$PHP_VERSION-raphf \
        php$PHP_VERSION-http \
        php$PHP_VERSION-imap \
        php$PHP_VERSION-dev \
        php$PHP_VERSION-imagick \
        php$PHP_VERSION-cgi \
        php$PHP_VERSION-opcache \
        php$PHP_VERSION-readline \
        php$PHP_VERSION-bcmath \
        php$PHP_VERSION-bz2 \
        php$PHP_VERSION-propro \
        php$PHP_VERSION-redis

ADD ./docker/conf/default.conf /etc/nginx/conf.d/default.conf
ADD ./docker/conf/www.conf /etc/php/$PHP_VERSION/fpm/pool.d/www.conf

#OCI 8
#ADD ./docker/oci8/tnsnames.ora $ORACLE_CONN
#ADD ./docker/oci8/sqlnet.ora $ORACLE_CONN

ENV OCI8_VERSION=2.2.0

RUN apt update && \
    apt install -y php-xml \
                   php-raphf 

RUN pecl channel-update pecl.php.net && \
    echo "instantclient,${LD_LIBRARY_PATH}" | pecl install oci8-$OCI8_VERSION

RUN echo "extension=oci8.so" > /etc/php/$PHP_VERSION/mods-available/oci8.ini && \
    ln -s /etc/php/$PHP_VERSION/mods-available/oci8.ini /etc/php/$PHP_VERSION/fpm/conf.d/20-oci8.ini && \
    ln -s /etc/php/$PHP_VERSION/mods-available/oci8.ini /etc/php/$PHP_VERSION/cli/conf.d/20-oci8.ini && \
    ln -s /etc/php/$PHP_VERSION/mods-available/oci8.ini /etc/php/$PHP_VERSION/cgi/conf.d/20-oci8.ini

RUN apt purge -y php8.*

#Composer
RUN EXPECTED_CHECKSUM="$(php -r 'copy("https://composer.github.io/installer.sig", "php://stdout");')" && \
    php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" && \
    php -r "if (hash_file('sha384', 'composer-setup.php') === '"$EXPECTED_CHECKSUM"') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;" && \
    php composer-setup.php --install-dir=/usr/bin --filename=composer && \
    php -r "unlink('composer-setup.php');"

#Supervisor
ADD ./docker/conf/php-supervisord.conf /tmp/supervisord.conf
RUN echo "\n" >> /etc/supervisord.conf && \
    cat /tmp/supervisord.conf >> /etc/supervisord.conf && \
    rm -f /tmp/supervisord.conf

#Removals
RUN apt-get remove -y \
    gnupg2 \
    software-properties-common \
    lsb-release \
    apt-transport-https

ADD ./docker/scripts/entrypoint.sh /entrypoint.sh
RUN sed -i -e 's/\r$//' /entrypoint.sh
RUN chmod 755 /entrypoint.sh

#Debugger
ENV PHP_DATE_VERSION=20151012
ENV XDEBUG_VERSION=2.6.1
ENV XDEBUG_CONTENT="\n\n[XDEBUG]\nzend_extension=\"/usr/lib/php/$PHP_DATE_VERSION/xdebug.so\""

RUN pecl install xdebug-$XDEBUG_VERSION && \
    echo -e $XDEBUG_CONTENT >> /etc/php/$PHP_VERSION/fpm/php.ini && \
    echo $XDEBUG_CONTENT > /etc/php/$PHP_VERSION/mods-available/xdebug.ini && \
    ln -s /etc/php/$PHP_VERSION/mods-available/xdebug.ini /etc/php/$PHP_VERSION/cli/conf.d/xdebug.ini && \
    echo "error_reporting=E_ALL" >> /etc/php/$PHP_VERSION/fpm/php.ini && \
    echo "display_startup_errors=On" >> /etc/php/$PHP_VERSION/fpm/php.ini && \
    echo "display_errors=On" >> /etc/php/$PHP_VERSION/fpm/php.ini && \
    echo "xdebug.mode=debug" >> /etc/php/$PHP_VERSION/fpm/php.ini && \
    echo "xdebug.client_host=host.docker.internal" >> /etc/php/$PHP_VERSION/fpm/php.ini && \
    echo "xdebug.client_port=9001" >> /etc/php/$PHP_VERSION/fpm/php.ini && \
    echo "xdebug.remote_handler=dbgp" >> /etc/php/$PHP_VERSION/fpm/php.ini && \
    echo "xdebug.start_with_request=yes" >> /etc/php/$PHP_VERSION/fpm/php.ini && \
    echo "xdebug.discover_client_host=false" >> /etc/php/$PHP_VERSION/fpm/php.ini && \
    echo "xdebug.idekey=docker" >> /etc/php/$PHP_VERSION/fpm/php.ini && \
    echo "openssl.capath=/etc/ssl/certs" >> /etc/php/$PHP_VERSION/fpm/php.ini

CMD ["/entrypoint.sh"]
